namespace PrisonerDilemma;

/// <summary>
/// Always defects
/// </summary>
class PrisonerBianca : IPrisoner
{
  public string Name { get; } = "Bianca";
  private PrisonerChoice previousChoice;
  private int snitched = 0;
  private int coop = 0;
  public void Reset(DilemmaParameters dilemmaParameters) {
      snitched = 0;
      coop = 0;
   }

  public PrisonerChoice GetChoice()
  {
    if (snitched > coop)
    {
        return PrisonerChoice.Defect;
    }
    else if (coop > snitched && previousChoice != PrisonerChoice.Defect)
    {
        return PrisonerChoice.Cooperate;
    }
    else
    {
      return PrisonerChoice.Defect;
    }
  }

  public void ReceiveOtherChoice(PrisonerChoice otherChoice) { 
    previousChoice = otherChoice;
    if (otherChoice == PrisonerChoice.Defect)
    {
        snitched++;
    }
    else {
        coop++;
    }
  }
}